package main

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"strconv"

	graphDB "gitlab.com/bondDB/graph"
	"gitlab.com/bondDB/graph/generators"

	"github.com/gorilla/mux"
)

var templates map[string]*template.Template
var graph graphDB.Grapher
var nIDGen, eIDGen generators.IdentityGenerator

// init does initialization before startup.
func init() {
	nIDGen = generators.NewIdentityGenerator(0)
	eIDGen = generators.NewIdentityGenerator(0)
	graph = graphDB.NewGraph(nIDGen, eIDGen)

	graph.AddNode("DOG")
	graph.AddNode("CAT")

	foo, _ := graph.AddNode("PERSON")
	foo.SetProperty("Name", []byte("Foo"))
	foo.SetProperty("Surname", []byte("Doo"))
	foo.SetProperty("Age", []byte("37"))

	bar, _ := graph.AddNode("PERSON")
	bar.SetProperty("Name", []byte("Bar"))
	foo.SetProperty("Surname", []byte("Berian"))
	foo.SetProperty("Age", []byte("40"))

	edge, _ := graph.AddEdge(foo, "LIKES", bar)
	edge.SetProperty("since", []byte("school"))
	edge.SetProperty("in", []byte("1995"))

	// custom functions
	tempFunc := template.FuncMap{
		"nodeCount": graph.NodeCount,
		"getNodes":  graph.GetNodes,
		"edgeCount": graph.EdgeCount,
		"getEdges":  graph.GetEdges,
	}

	templates = make(map[string]*template.Template)
	templates["index"] = template.Must(
		template.New("layout").Funcs(tempFunc).ParseFiles(
			"../templates/base.html",
			"../templates/index.html",
			"../templates/main-cards.html",
			"../templates/right-card.html",
			"../templates/left-card.html",
			"../templates/left-card-body.html",
			"../templates/sidebar-links.html",
		),
	)
	templates["nodes"] = template.Must(
		template.New("layout").Funcs(tempFunc).ParseFiles(
			"../templates/base.html",
			"../templates/index.html",
			"../templates/sidebar-links.html",
			"../templates/nodeEdgesBase.html",
			"../templates/nodes.html",
		),
	)
	templates["edges"] = template.Must(
		template.New("layout").Funcs(tempFunc).ParseFiles(
			"../templates/base.html",
			"../templates/index.html",
			"../templates/sidebar-links.html",
			"../templates/nodeEdgesBase.html",
			"../templates/edges.html",
		),
	)
	templates["node"] = template.Must(
		template.New("layout").Funcs(tempFunc).ParseFiles(
			"../templates/base.html",
			"../templates/main-cards.html",
			"../templates/sidebar-links.html",
			"../templates/node.html",
		),
	)

}

// Index returns the index page.
func Index(w http.ResponseWriter, r *http.Request) {
	tmpl := templates["index"]
	err := tmpl.ExecuteTemplate(w, "base", nil)
	if err != nil {
		panic(err)
	}
}

// Node will return the node in JSON format.
func Node(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	vars := mux.Vars(r)

	id, err := strconv.ParseUint(vars["id"], 10, 10)
	if err != nil {
		http.Error(w, err.Error(), 404)
	}

	node, err := graph.GetNode(id)
	if err != nil {
		http.Error(w, err.Error(), 404)
	}

	json.NewEncoder(w).Encode(node)
}

// Nodes will return all the nodes in JSON format.
func Nodes(w http.ResponseWriter, r *http.Request) {
	nodes := graph.GetNodes()
	if r.Header.Get("Content-Type") == "application/json" {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(nodes)
	} else {

		tmpl := templates["nodes"]
		err := tmpl.ExecuteTemplate(w, "base", nil)
		if err != nil {
			http.Error(w, err.Error(), 502)
		}
	}
}

// Edge will return the edge in JSON format.
func Edge(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	vars := mux.Vars(r)

	id, err := strconv.ParseUint(vars["id"], 10, 10)
	if err != nil {
		http.Error(w, err.Error(), 404)
	}

	edge, err := graph.GetEdge(id)
	if err != nil {
		http.Error(w, err.Error(), 404)
	}

	json.NewEncoder(w).Encode(edge)
}

// Edges will return all the edges in JSON format.
func Edges(w http.ResponseWriter, r *http.Request) {
	edges := graph.GetEdges()
	if r.Header.Get("Content-Type") == "application/json" {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(edges)
	} else {

		tmpl := templates["edges"]
		err := tmpl.ExecuteTemplate(w, "base", nil)
		if err != nil {
			http.Error(w, err.Error(), 502)
		}
	}
}

// main is the main entry point.
func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", Index)
	router.HandleFunc("/nodes", Nodes)
	router.HandleFunc("/nodes/{id}", Node)
	router.HandleFunc("/edges", Edges)
	router.HandleFunc("/edges/{id}", Edge)

	s := http.StripPrefix(
		"/static/",
		http.FileServer(http.Dir("../static/")),
	)

	router.PathPrefix("/static/").Handler(s)

	log.Fatal(http.ListenAndServe("0.0.0.0:8001", router))
}
