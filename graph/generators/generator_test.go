package generators

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewIdentityGenerator(t *testing.T) {
	idGen := NewIdentityGenerator(0)
	assert.Implements(t, (*IdentityGenerator)(nil), idGen)
}

func TestNewIdentityGeneratorStartAtTen(t *testing.T) {
	idGen := NewIdentityGenerator(10)
	assert.Exactly(t, uint64(10), idGen.Current())
}

func TestIDGeneratorCurrent(t *testing.T) {
	idGen := NewIdentityGenerator(0)
	assert.Exactly(t, uint64(0), idGen.Current())
}

func TestIDGeneratorNext(t *testing.T) {
	idGen := NewIdentityGenerator(0)
	assert.Exactly(t, uint64(1), idGen.Next())
	assert.Exactly(t, uint64(2), idGen.Next())
	assert.Exactly(t, uint64(3), idGen.Next())
}

func TestIDGeneratorNextStartingFromTen(t *testing.T) {
	idGen := NewIdentityGenerator(10)
	assert.Exactly(t, uint64(11), idGen.Next())
	assert.Exactly(t, uint64(12), idGen.Next())
	assert.Exactly(t, uint64(13), idGen.Next())
}

func TestIDGeneratorRollback(t *testing.T) {
	idGen := NewIdentityGenerator(0)

	idGen.Next() // 1
	idGen.Next() // 2
	idGen.Next() // 3

	assert.Exactly(t, uint64(2), idGen.Rollback())
	assert.Exactly(t, uint64(1), idGen.Rollback())
	assert.Exactly(t, uint64(0), idGen.Rollback())
}

func TestIDGeneratorRollbackPastZero(t *testing.T) {
	idGen := NewIdentityGenerator(0)

	assert.Exactly(t, uint64(0), idGen.Rollback())
	assert.Exactly(t, uint64(0), idGen.Rollback())
	assert.Exactly(t, uint64(0), idGen.Rollback())
}
