package generators

// IdentityGenerator is an interface wrapping methods used
// for generating identity numbers.
type IdentityGenerator interface {
	// Current returns the current identity number.
	Current() uint64

	// Next increments and returns a new identity number.
	Next() uint64

	// Rollback decrements and returns the previous identity number.
	Rollback() uint64
}

// NewIdentityGenerator creates a new identity generator
// starting at the given `id`.
func NewIdentityGenerator(id uint64) *IDGenerator {
	idGen := IDGenerator{
		id: id,
	}
	return &idGen
}

// IDGenerator is a simple ID generator.
type IDGenerator struct {
	id uint64
}

// Current returns the current identity number.
func (i *IDGenerator) Current() uint64 {
	return i.id
}

// Next increments and returns a new identity number.
func (i *IDGenerator) Next() uint64 {
	i.id++
	return i.id
}

// Rollback decrements and returns the previous identity number.
func (i *IDGenerator) Rollback() uint64 {
	if i.id <= 0 {
		return 0
	}
	i.id--
	return i.id
}
