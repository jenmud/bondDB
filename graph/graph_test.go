package graph

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bondDB/graph/component"
	"gitlab.com/bondDB/graph/generators"
	"gitlab.com/bondDB/graph/index"
)

func makeEmptyGraph() *Graph {
	return NewGraph(
		generators.NewIdentityGenerator(0),
		generators.NewIdentityGenerator(0),
	)
}

func ExampleGraph_AddNode() {
	graph := NewGraph(
		generators.NewIdentityGenerator(0),
		generators.NewIdentityGenerator(0),
	)

	node, err := graph.AddNode("PERSON")
	if err != nil {
		panic(err)
	}

	node.SetProperty("name", []byte("Foo"))
}

func ExampleGraph_AddEdge() {
	graph := NewGraph(
		generators.NewIdentityGenerator(0),
		generators.NewIdentityGenerator(0),
	)

	foo, err := graph.AddNode("PERSON")
	if err != nil {
		panic(err)
	}

	bar, err := graph.AddNode("PERSON")
	if err != nil {
		panic(err)
	}

	_, err = graph.AddEdge(foo, "knows", bar)
	if err != nil {
		panic(err)
	}
}

func TestGraph_NewGraph(t *testing.T) {
	graph := NewGraph(
		generators.NewIdentityGenerator(0),
		generators.NewIdentityGenerator(0),
	)
	assert.Implements(t, (*Grapher)(nil), graph)
}

func TestGraph_AddNode(t *testing.T) {
	graph := makeEmptyGraph()
	node, err := graph.AddNode("PERSON")
	assert.Nil(t, err)
	assert.Implements(t, (*component.NodeComponent)(nil), node)
}

func TestGraph_AddNode_Binding(t *testing.T) {
	graph := makeEmptyGraph()
	node, _ := graph.AddNode("PERSON")
	assert.Equal(t, true, node.IsBound())
}

func TestGraph_AddNode_IndexUpdated(t *testing.T) {
	graph := makeEmptyGraph()

	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	bob, _ := graph.AddNode("PET")

	// Check that the node is indexed.
	data, err := graph.nodeLabelIndex.Get("PERSON")
	// label key has been created in the index trie tree
	assert.Nil(t, err)

	// Ugly yes, but because I am using an interface,
	// I need to type assert it.
	// Todo: fix me
	nodeSet := data.(map[uint64]component.GraphComponent)
	assert.Equal(t, foo, nodeSet[foo.GetID()])
	assert.Equal(t, bar, nodeSet[bar.GetID()])

	data, err = graph.nodeLabelIndex.Get("PET")
	assert.Nil(t, err)
	nodeSet = data.(map[uint64]component.GraphComponent)
	assert.Equal(t, bob, nodeSet[bob.GetID()])
}

func TestGraph_FindNodesByLabel(t *testing.T) {
	graph := makeEmptyGraph()

	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	bob, _ := graph.AddNode("PET")

	results := graph.FindNodesByLabel("PERSON")
	assert.Equal(t, foo.GetID(), results[0].GetID())
	assert.Equal(t, bar.GetID(), results[1].GetID())

	results = graph.FindNodesByLabel("PET")
	assert.Equal(t, bob, results[0])

	results = graph.FindNodesByLabel("UNKNOWN")
	assert.Equal(t, []component.GraphComponent{}, results)
}

func TestGraph_indexInsert(t *testing.T) {
	rootIndex := index.NewTTree()
	node := component.NewNode(0, "PERSON")
	indexInsert(rootIndex, "PERSON", node)

	data, err := rootIndex.Get("PERSON")
	assert.Nil(t, err)
	result := data.(map[uint64]component.GraphComponent)
	assert.Equal(t, node, result[node.GetID()])
}

func TestGraph_findNodes(t *testing.T) {
	rootIndex := index.NewTTree()
	node := component.NewNode(0, "PERSON")
	indexInsert(rootIndex, "PERSON", node)

	result := findNodes(rootIndex, "PERSON")
	expected := []component.GraphComponent{node}
	assert.Equal(t, expected, result)

	result = findNodes(rootIndex, "UNKNOWN")
	expected = []component.GraphComponent{}
	assert.Equal(t, expected, result)
}

func TestGraph_RemoveNode(t *testing.T) {
	graph := makeEmptyGraph()
	graph.AddNode("PERSON")
	graph.AddNode("PERSON")
	err := graph.RemoveNode(1)
	assert.Nil(t, err)
	assert.Equal(t, 1, graph.NodeCount())
}

func TestGraph_indexRemove(t *testing.T) {
	rootIndex := index.NewTTree()

	node := component.NewNode(0, "PERSON")
	indexInsert(rootIndex, "PERSON", node)

	// remove node which is the only node in the `PERSON`
	// index and therefor the entire branch should be removed.
	indexRemove(rootIndex, "PERSON", node)
	assert.Equal(t, false, rootIndex.Contains("PERSON"))

	// Lets add multiple `PERSON` nodes to the index
	// and remove it making sure that `bar` is still
	// accessible and the entire `PERSON` branch still
	// exists after `foo` removal
	foo := component.NewNode(1, "PERSON")
	bar := component.NewNode(2, "PERSON")
	indexInsert(rootIndex, "PERSON", foo)
	indexInsert(rootIndex, "PERSON", bar)

	indexRemove(rootIndex, "PERSON", foo)
	assert.Equal(t, true, rootIndex.Contains("PERSON"))

	data, err := rootIndex.Get("PERSON")
	assert.Nil(t, err)
	nodes := data.(map[uint64]component.GraphComponent)
	_, ok := nodes[bar.GetID()]
	assert.Equal(t, true, ok)
}

func TestGraph_RemoveNode_Missing_Node(t *testing.T) {
	graph := makeEmptyGraph()
	graph.AddNode("PERSON")
	graph.AddNode("PERSON")
	err := graph.RemoveNode(4)
	assert.Error(t, err)
	assert.Equal(t, 2, graph.NodeCount())
}

func TestGraph_RemoveNodeLinkedNode(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	graph.AddEdge(foo, "KNOWS", bar)
	err := graph.RemoveNode(1)
	assert.Error(t, err)
	assert.Equal(t, 2, graph.NodeCount())
}

func TestGraph_GetNode(t *testing.T) {
	graph := makeEmptyGraph()
	orgNode, _ := graph.AddNode("PERSON")
	node, err := graph.GetNode(1)
	assert.Nil(t, err)
	assert.Equal(t, orgNode, node)
}

func TestGraph_GetNode_Missing_ID(t *testing.T) {
	graph := makeEmptyGraph()
	graph.AddNode("PERSON")
	node, err := graph.GetNode(10)
	assert.Error(t, err)
	assert.Nil(t, node)
}

func TestGraph_NodeCount(t *testing.T) {
	graph := makeEmptyGraph()
	graph.AddNode("PERSON")
	graph.AddNode("PERSON")
	assert.Equal(t, graph.NodeCount(), 2)
}

func TestGraph_HasNode(t *testing.T) {
	graph := makeEmptyGraph()
	graph.AddNode("PERSON")
	graph.AddNode("PERSON")
	assert.Equal(t, graph.HasNode(2), true)
	assert.Equal(t, graph.HasNode(3), false)
}

func TestGraph_AddEdge(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	edge, err := graph.AddEdge(foo, "KNOWS", bar)
	assert.Nil(t, err)
	assert.Implements(t, (*component.EdgeComponent)(nil), edge)

	// Test that the nodes are linked together
	assert.Equal(t, foo.GetOutEdges()[0], edge)
	assert.Equal(t, bar.GetInEdges()[0], edge)
}

func TestGraph_AddEdge_Binding(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	edge, _ := graph.AddEdge(foo, "KNOWS", bar)
	assert.Equal(t, true, edge.IsBound())
}

func TestGraph_AddEdge_IndexUpdated(t *testing.T) {
	graph := makeEmptyGraph()

	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")

	edge, err := graph.AddEdge(foo, "KNOWS", bar)
	assert.Nil(t, err)

	// Check that the node is indexed.
	data, err := graph.edgeLabelIndex.Get("KNOWS")
	assert.Nil(t, err)

	// Ugly yes, but because I am using an interface, I need to type cast it.
	// Todo: fix me
	edgeSet := data.(map[uint64]component.GraphComponent)
	assert.Equal(t, edge, edgeSet[edge.GetID()])
}

func TestGraph_RemoveEdge(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	graph.AddEdge(foo, "KNOWS", bar)

	err := graph.RemoveEdge(1)
	assert.Nil(t, err)
	assert.Equal(t, 0, graph.EdgeCount())

	// Test that nodes are no longer linked together
	assert.Equal(t, 0, len(foo.GetOutEdges()))
	assert.Equal(t, 0, len(bar.GetInEdges()))
}

func TestGraph_RemoveEdge_Unknown(t *testing.T) {
	graph := makeEmptyGraph()
	err := graph.RemoveEdge(1)
	assert.Error(t, err)
}

func TestGraph_GetEdge(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	orgEdge, _ := graph.AddEdge(foo, "KNOWS", bar)
	edge, err := graph.GetEdge(1)
	assert.Nil(t, err)
	assert.Equal(t, orgEdge, edge)
}

func TestGraph_GetEdge_Missing_ID(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	graph.AddEdge(foo, "KNOWS", bar)
	edge, err := graph.GetEdge(10)
	assert.Error(t, err)
	assert.Nil(t, edge)
}

func TestGraph_AddEdge_Missing_Nodes(t *testing.T) {
	graph := makeEmptyGraph()

	bar, _ := graph.AddNode("PERSON")
	foo := component.NewNode(2, "PERSON")

	// fail missing head node
	edge, err := graph.AddEdge(foo, "KNOWS", bar)
	assert.Error(t, err)
	assert.Nil(t, edge)

	// fail missing tail node
	edge, err = graph.AddEdge(bar, "KNOWS", foo)
	assert.Error(t, err)
	assert.Nil(t, edge)
}

func TestGraph_EdgeCount(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	graph.AddEdge(foo, "KNOWS", bar)
	assert.Equal(t, graph.EdgeCount(), 1)
}

func TestGraph_HasEdge(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	graph.AddEdge(foo, "KNOWS", bar)
	assert.Equal(t, graph.HasEdge(1), true)
	assert.Equal(t, graph.HasEdge(2), false)
}

func TestGraph_GetNodes(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	graph.AddEdge(foo, "KNOWS", bar)
	expected := []component.NodeComponent{foo, bar}
	assert.Equal(t, expected, graph.GetNodes())
}

func TestGraph_GetEdges(t *testing.T) {
	graph := makeEmptyGraph()
	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	edgeA, _ := graph.AddEdge(foo, "KNOWS", bar)
	edgeB, _ := graph.AddEdge(bar, "LIKES", foo)
	expected := []component.EdgeComponent{edgeA, edgeB}
	assert.Equal(t, expected, graph.GetEdges())
}

func TestGraph_FindEdgesByLabel(t *testing.T) {
	graph := makeEmptyGraph()

	foo, _ := graph.AddNode("PERSON")
	bar, _ := graph.AddNode("PERSON")
	fooKnowsBar, _ := graph.AddEdge(foo, "KNOWS", bar)

	results := graph.FindEdgesByLabel("KNOWS")
	expected := []component.GraphComponent{fooKnowsBar}
	assert.Equal(t, expected, results)

	results = graph.FindEdgesByLabel("UNKNOWN")
	expected = []component.GraphComponent{}
	assert.Equal(t, expected, results)
}
