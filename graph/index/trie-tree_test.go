//
// bike
// ----
//
// b - i - k - e (IsEnd: True, Data: bike)
//
//
// bus
// ---
//    _ i - k - e (IsEnd: True, Data: bike)
//   |
// b -
//   |_ u - s (IsEnd: True, Data: bus)
//
//
// bicycle
// -------
//
//       _ k - e (IsEnd: True, Data: bike)
//      |
//    _ i
//   |  |_ c - y - c - l - e (IsEnd: True, Data: bicycle)
// b -
//   |_ u - s (IsEnd: True, Data: bus)
//
//
package index

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTTree_Insert(t *testing.T) {
	tree := NewTTree()
	// root
	//  |_ b - u - s (IsEnd: True, Data "bus")
	trie, err := tree.Insert("bus", "bus")
	assert.Nil(t, err)
	assert.Equal(t, "bus", trie.Data)
	assert.Equal(t, "u", trie.Parent.Value)

	// root
	//  |   _ i - k - e (IsEnd: True, Data: bike)
	//  |  |
	//  |_ b -
	//     |_ u - s (IsEnd: True, Data: bus)
	trie, err = tree.Insert("bike", "bike")
	assert.Nil(t, err)
	assert.Equal(t, "bike", trie.Data)
}

func TestTTree_Get(t *testing.T) {
	tree := NewTTree()

	// root
	//  |   _ i - k - e (IsEnd: True, Data: bike)
	//  |  |
	//  |_ b -
	//     |_ u - s (IsEnd: True, Data: bus)
	_, err := tree.Insert("bus", "bus")
	assert.Nil(t, err)

	_, err = tree.Insert("bike", "bike")
	assert.Nil(t, err)

	value, err := tree.Get("bus")
	assert.Nil(t, err)
	assert.Equal(t, "bus", value)

	value, err = tree.Get("bike")
	assert.Nil(t, err)
	assert.Equal(t, "bike", value)

	value, err = tree.Get("busses")
	assert.Nil(t, value)
	assert.Error(t, err)
}

func TestTTree_GetLeaf(t *testing.T) {
	tree := NewTTree()

	// root
	//  |   _ i - k - e (IsEnd: True, Data: bike)
	//  |  |
	//  |_ b -
	//     |_ u - s (IsEnd: True, Data: bus)
	expected, err := tree.Insert("bus", "bus")
	assert.Nil(t, err)

	actual, err := tree.GetLeaf("bus")
	assert.Nil(t, err)
	assert.Equal(t, expected, actual)
}

func TestTTree_Contains(t *testing.T) {
	tree := NewTTree()

	// root
	//  |   _ i - k - e (IsEnd: True, Data: bike)
	//  |  |
	//  |_ b -
	//     |_ u - s (IsEnd: True, Data: bus)
	_, err := tree.Insert("bus", "bus")
	assert.Nil(t, err)

	_, err = tree.Insert("bike", "bike")
	assert.Nil(t, err)

	assert.Equal(t, true, tree.Contains("bus"))
	assert.Equal(t, true, tree.Contains("bike"))
	assert.Equal(t, false, tree.Contains("busses"))
}

func TestTTree_Count(t *testing.T) {
	tree := NewTTree()

	// root
	//  |   _ i - k - e (IsEnd: True, Data: bike)
	//  |  |
	//  |_ b -
	//     |_ u - s (IsEnd: True, Data: bus)
	_, err := tree.Insert("bus", "bus")
	assert.Nil(t, err)

	_, err = tree.Insert("bike", "bike")
	assert.Nil(t, err)

	assert.Equal(t, 2, tree.Count())
}

func TestTTree_Remove(t *testing.T) {
	tree := NewTTree()

	// root
	//  |   _ i - k - e (IsEnd: True, Data: bike)
	//  |  |
	//  |_ b -
	//     |_ u - s (IsEnd: True, Data: bus)
	//            |
	//            |_ s - e - s (IsEnd: True, Data: busses)

	_, err := tree.Insert("bus", "bus")
	assert.Nil(t, err)

	_, err = tree.Insert("bike", "bike")
	assert.Nil(t, err)

	_, err = tree.Insert("busses", "busses")
	assert.Nil(t, err)

	err = tree.Remove("bus")
	assert.Nil(t, err)

	assert.Equal(t, false, tree.Contains("bus"))
	assert.Equal(t, true, tree.Contains("bike"))
	assert.Equal(t, true, tree.Contains("busses"))

	err = tree.Remove("bu")
	assert.Error(t, err)
}
