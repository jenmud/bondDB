package index

import "fmt"

func NewTrie() *Trie {
	trie := Trie{
		Parent: &Trie{},
		IsEnd:  false,
		Leafs:  make(map[string]*Trie),
	}

	return &trie
}

type Trie struct {
	Parent *Trie
	Value  string
	Data   interface{}
	IsEnd  bool
	Leafs  map[string]*Trie
}

func NewTTree() *TTree {
	root := NewTrie()
	root.Value = "root"

	tree := TTree{
		root: root,
	}

	return &tree
}

type TTree struct {
	root  *Trie
	count int
}

func insertTrie(leaf *Trie, key string, data interface{}) (*Trie, error) {
	if leaf == nil {
		return nil, fmt.Errorf("Leaf can not be nil")
	}

	if len(key) == 0 {
		leaf.IsEnd = true
		leaf.Data = data
		return leaf, nil
	}

	// bike -> original key
	// b ike
	// b i ke
	// b i k e -> is end

	head := string(key[0])
	tail := string(key[1:])

	l, ok := leaf.Leafs[head]
	if !ok {
		l = NewTrie()
		l.Value = head
		l.Parent = leaf
		leaf.Leafs[head] = l
	}

	return insertTrie(l, tail, data)
}

func (t *TTree) Insert(key string, data interface{}) (*Trie, error) {
	leaf, err := insertTrie(t.root, key, data)
	if err != nil {
		return leaf, err
	}
	t.count++
	return leaf, err
}

func removeTrie(leaf *Trie, key string) (*Trie, error) {

	// Todo: look at pruning up the tree if you are removing
	//       a leaf the none of the parent leaf's are used.
	//
	//	 Example:
	//       root
	//	  |
	//        |_ b - u - s (IsEnd: True, Data: bus)
	//
	//	 Result:
	//	  root
	//         |_

	// Deleting bus
	// root
	//  |
	//  |_ b
	//     |_ u - s (IsEnd: True, Data: bus)
	//            |
	//            |_ s - e - s (IsEnd: True, Data: busses)

	if len(key) == 0 && leaf.IsEnd {
		leaf.IsEnd = false
		leaf.Data = nil
		if len(leaf.Leafs) == 0 {
			delete(leaf.Parent.Leafs, leaf.Value)
		}
		return nil, nil
	}

	head := string(key[0])
	tail := string(key[1:])

	l, _ := leaf.Leafs[head]
	return removeTrie(l, tail)
}

func (t *TTree) Remove(key string) error {
	if !t.Contains(key) {
		return fmt.Errorf("Could not find key %q", key)
	}

	_, err := removeTrie(t.root, key)
	return err
}

func getTrie(leaf *Trie, key string) (*Trie, error) {
	if len(key) == 0 {
		if leaf.IsEnd {
			return leaf, nil
		}
		return nil, fmt.Errorf("Could not find key %q", key)
	}

	head := string(key[0])
	tail := string(key[1:])

	l, ok := leaf.Leafs[head]
	if !ok {
		return nil, fmt.Errorf("Could not find key %q", key)
	}

	return getTrie(l, tail)
}

func (t *TTree) GetLeaf(key string) (interface{}, error) {
	return getTrie(t.root, key)
}

func (t *TTree) Get(key string) (interface{}, error) {
	leaf, err := getTrie(t.root, key)
	if err != nil {
		return nil, err
	}
	return leaf.Data, nil
}

func (t *TTree) Contains(key string) bool {
	_, err := t.Get(key)
	return err == nil
}

func (t *TTree) Count() int {
	return t.count
}
