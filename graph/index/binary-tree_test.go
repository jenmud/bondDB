package index

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func make_complext_tree() *BTree {
	b := NewBTree()

	b.Insert(1, "one")
	b.Insert(3, "three")
	b.Insert(2, "two")
	b.Insert(4, "four")
	b.Insert(8, "eight")
	b.Insert(6, "six")
	b.Insert(0, "zero")
	b.Insert(10, "ten")
	b.Insert(5, "five")
	b.Insert(9, "nine")
	b.Insert(7, "seven")

	// 0
	//  \
	//   1
	//   /\
	//  0  3
	//     /\
	//    2  4
	//        \
	//         8
	//         /\
	//        6  10
	//       /\  /
	//      5  7 9

	return b
}

func TestBTree_insert(t *testing.T) {
	root := newLeaf()

	two := newLeaf()
	two.ID = 2
	two.Data = "two"

	one := newLeaf()
	one.ID = 1
	one.Data = "one"

	three := newLeaf()
	three.ID = 3
	three.Data = "three"

	four := newLeaf()
	four.ID = 4
	four.Data = "four"

	root, err := insert(root, nil)
	assert.Error(t, err)

	//   2
	//  /
	// 1
	one, err = insert(two, one)
	assert.Nil(t, err)
	assert.Equal(t, two, one.Parent)
	assert.Equal(t, one, two.Left)

	//   2
	//  /\
	// 1  3
	three, err = insert(two, three)
	assert.Nil(t, err)
	assert.Equal(t, two, three.Parent)
	assert.Equal(t, three, two.Right)

	//   2
	//  /\
	// 1  3
	//     \
	//      4
	four, err = insert(two, four)
	assert.Nil(t, err)
	assert.Equal(t, three, four.Parent)
	assert.Equal(t, four, three.Right)
}

func TestBTree_Insert(t *testing.T) {
	// The following is a random insert from 1-10

	// Trees root always starts with ID 0
	tree := NewBTree()
	root := tree.GetRoot()

	// 0
	//  \
	//   1
	one, err := tree.Insert(1, "one")
	assert.Nil(t, err)
	assert.Equal(t, one, root.Right)
	assert.Equal(t, root, one.Parent)
	assert.Equal(t, "one", one.Data)
	assert.Equal(t, uint64(1), one.ID)

	// 0
	//  \
	//   1
	//    \
	//     3
	three, err := tree.Insert(3, "three")
	assert.Nil(t, err)
	assert.Equal(t, three, one.Right)
	assert.Equal(t, one, three.Parent)
	assert.Equal(t, "three", three.Data)
	assert.Equal(t, uint64(3), three.ID)

	// 0
	//  \
	//   1
	//    \
	//     3
	//    /
	//   2
	two, err := tree.Insert(2, "two")
	assert.Nil(t, err)
	assert.Equal(t, two, three.Left)
	assert.Equal(t, three, two.Parent)
	assert.Equal(t, "two", two.Data)
	assert.Equal(t, uint64(2), two.ID)

	// 0
	//  \
	//   1
	//    \
	//     3
	//    /\
	//   2  4
	four, err := tree.Insert(4, "four")
	assert.Nil(t, err)
	assert.Equal(t, four, three.Right)
	assert.Equal(t, three, four.Parent)
	assert.Equal(t, "four", four.Data)
	assert.Equal(t, uint64(4), four.ID)

	// 0
	//  \
	//   1
	//    \
	//     3
	//    /\
	//   2  4
	//       \
	//        8
	eight, err := tree.Insert(8, "eight")
	assert.Nil(t, err)
	assert.Equal(t, eight, four.Right)
	assert.Equal(t, four, eight.Parent)
	assert.Equal(t, "eight", eight.Data)
	assert.Equal(t, uint64(8), eight.ID)

	// 0
	//  \
	//   1
	//    \
	//     3
	//    /\
	//   2  4
	//       \
	//        8
	//       /
	//      6
	six, err := tree.Insert(6, "six")
	assert.Nil(t, err)
	assert.Equal(t, six, eight.Left)
	assert.Equal(t, eight, six.Parent)
	assert.Equal(t, "six", six.Data)
	assert.Equal(t, uint64(6), six.ID)

	// 0
	//  \
	//   1
	//   /\
	//     3  --> 0 should NOT be inserted on the left of 1
	//     /\
	//    2  4
	//        \
	//         8
	//        /
	//       6
	zero, err := tree.Insert(0, "zero")
	assert.Nil(t, zero)
	assert.Error(t, err)

	// 0
	//  \
	//   1
	//   /\
	//     3
	//     /\
	//    2  4
	//        \
	//         8
	//         /\
	//        6 10
	ten, err := tree.Insert(10, "ten")
	assert.Nil(t, err)
	assert.Equal(t, ten, eight.Right)
	assert.Equal(t, eight, ten.Parent)
	assert.Equal(t, "ten", ten.Data)
	assert.Equal(t, uint64(10), ten.ID)

	// 0
	//  \
	//   1
	//   /\
	//     3
	//     /\
	//    2  4
	//        \
	//         8
	//         /\
	//        6 10
	//       /
	//      5
	five, err := tree.Insert(5, "five")
	assert.Nil(t, err)
	assert.Equal(t, five, six.Left)
	assert.Equal(t, six, five.Parent)
	assert.Equal(t, "five", five.Data)
	assert.Equal(t, uint64(5), five.ID)

	// 0
	//  \
	//   1
	//   /\
	//     3
	//     /\
	//    2  4
	//        \
	//         8
	//         /\
	//        6 10
	//       /  /
	//      5  9
	nine, err := tree.Insert(9, "nine")
	assert.Nil(t, err)
	assert.Equal(t, nine, ten.Left)
	assert.Equal(t, ten, nine.Parent)
	assert.Equal(t, "nine", nine.Data)
	assert.Equal(t, uint64(9), nine.ID)

	// 0
	//  \
	//   1
	//   /\
	//     3
	//     /\
	//    2  4
	//        \
	//         8
	//         /\
	//        6  10
	//       /\  /
	//      5  7 9
	seven, err := tree.Insert(7, "seven")
	assert.Nil(t, err)
	assert.Equal(t, seven, six.Right)
	assert.Equal(t, six, seven.Parent)
	assert.Equal(t, "seven", seven.Data)
	assert.Equal(t, uint64(7), seven.ID)
}

func TestBTree_InsertLeaf(t *testing.T) {
	// Trees root always starts with ID 0
	tree := NewBTree()

	// 0
	//  \
	//   2
	two, err := tree.Insert(2, "two")

	one := newLeaf()
	one.ID = 1
	one.Data = "one"

	three := newLeaf()
	three.ID = 3
	three.Data = "three"

	// 0
	//  \
	//   2
	//  /
	// 1
	one, err = tree.InsertLeaf(one)
	assert.Nil(t, err)
	assert.Equal(t, one, two.Left)
	assert.Equal(t, two, one.Parent)
	assert.Equal(t, "one", one.Data)
	assert.Equal(t, uint64(1), one.ID)

	// 0
	//  \
	//   2
	//  /\
	// 1  3
	three, err = tree.InsertLeaf(three)
	assert.Nil(t, err)
	assert.Equal(t, three, two.Right)
	assert.Equal(t, two, three.Parent)
	assert.Equal(t, "three", three.Data)
	assert.Equal(t, uint64(3), three.ID)
}

func TestLeaf_Get(t *testing.T) {
	tree := make_complext_tree()

	data, err := tree.Get(2)
	assert.Nil(t, err)
	assert.Equal(t, "two", data)

	data, err = tree.Get(3)
	assert.Nil(t, err)
	assert.Equal(t, "three", data)

	data, err = tree.Get(2)
	assert.Nil(t, err)
	assert.Equal(t, "two", data)

	data, err = tree.Get(7)
	assert.Nil(t, err)
	assert.Equal(t, "seven", data)

	data, err = tree.Get(9)
	assert.Nil(t, err)
	assert.Equal(t, "nine", data)

	data, err = tree.Get(11)
	assert.Nil(t, data)
	assert.Error(t, err)
}

func TestLeaf_Contains(t *testing.T) {
	tree := make_complext_tree()

	assert.Equal(t, true, tree.Contains(5))
	assert.Equal(t, true, tree.Contains(7))
	assert.Equal(t, true, tree.Contains(2))

	assert.Equal(t, false, tree.Contains(11))
}

func TestLeaf_Remove(t *testing.T) {
	tree := NewBTree()

	// 2
	// /\
	//1  3
	tree.Insert(2, "two")
	tree.Insert(1, "one")
	tree.Insert(3, "three")

	// 2
	// /
	//1
	err := tree.Remove(3)
	assert.Nil(t, err)

	assert.Equal(t, false, tree.Contains(3))

	one, err := tree.GetLeaf(1)
	assert.Nil(t, err)
	assert.Equal(t, uint64(2), one.Parent.ID)

	// Reset the tree

	// 2
	// /\
	//1  3
	//    \
	//     4
	tree = NewBTree()
	tree.Insert(2, "two")
	tree.Insert(1, "one")
	tree.Insert(3, "three")
	tree.Insert(4, "four")

	// 2
	// /\
	//1  4
	err = tree.Remove(3)
	assert.Nil(t, err)
	assert.Equal(t, false, tree.Contains(3))

	assert.Equal(t, false, tree.Contains(3))
	assert.Equal(t, true, tree.Contains(4))

	four, err := tree.GetLeaf(4)
	assert.Nil(t, err)
	assert.Equal(t, uint64(2), four.Parent.ID)

	one, err = tree.GetLeaf(1)
	assert.Nil(t, err)
	assert.Equal(t, uint64(2), one.Parent.ID)

	// Reset the tree

	//   2
	//   /\
	//  4  5
	//  /
	// 1
	tree = NewBTree()
	tree.Insert(2, "two")
	tree.Insert(4, "four")
	tree.Insert(1, "one")
	tree.Insert(5, "five")

	// 2
	// /\
	//1  5
	err = tree.Remove(4)
	assert.Nil(t, err)
	assert.Equal(t, false, tree.Contains(4))

	assert.Equal(t, false, tree.Contains(4))
	assert.Equal(t, true, tree.Contains(1))

	one, err = tree.GetLeaf(1)
	assert.Nil(t, err)
	assert.Equal(t, uint64(2), one.Parent.ID)

	five, err := tree.GetLeaf(5)
	assert.Nil(t, err)
	assert.Equal(t, uint64(2), five.Parent.ID)

	// Reset the tree

	//   4
	//   /\
	//  3  5
	//  /
	// 1
	//  \
	//   2
	tree = NewBTree()
	tree.Insert(4, "four")
	tree.Insert(3, "three")
	tree.Insert(1, "one")
	tree.Insert(2, "two")
	tree.Insert(5, "five")

	// 4
	// /\
	//1  5
	// \
	//  2
	err = tree.Remove(3)
	assert.Nil(t, err)
	assert.Equal(t, false, tree.Contains(3))
	assert.Equal(t, true, tree.Contains(1))
	assert.Equal(t, true, tree.Contains(2))

	one, err = tree.GetLeaf(1)
	assert.Nil(t, err)
	assert.Equal(t, uint64(4), one.Parent.ID)

	two, err := tree.GetLeaf(2)
	assert.Nil(t, err)
	assert.Equal(t, uint64(1), two.Parent.ID)

	five, err = tree.GetLeaf(5)
	assert.Nil(t, err)
	assert.Equal(t, uint64(4), five.Parent.ID)

	// Reset the tree

	// 2
	// /\
	//1  3
	//   /\
	//     6
	//     /\
	//    4  7
	tree = NewBTree()
	tree.Insert(2, "two")
	tree.Insert(1, "one")
	three, _ := tree.Insert(3, "three") // three is for later use
	tree.Insert(6, "six")
	tree.Insert(4, "four")
	tree.Insert(7, "seven")

	err = tree.Remove(6)
	assert.Nil(t, err)
	assert.Equal(t, false, tree.Contains(6))
	// After removed leaf 6
	// 2
	// /\
	//1  3
	//   /\
	//  4  7
	seven, err := tree.GetLeaf(7)
	assert.Nil(t, err)
	assert.Equal(t, uint64(3), seven.Parent.ID)
	assert.Equal(t, three, seven.Parent)

	four, err = tree.GetLeaf(4)
	assert.Nil(t, err)
	assert.Equal(t, uint64(7), four.Parent.ID)
	assert.Equal(t, seven, four.Parent)

	assert.Error(t, tree.Remove(11))
}

func TestBTree_Count(t *testing.T) {
	tree := make_complext_tree()
	assert.Equal(t, 10, tree.Count())

	// Test count is decremented with remove
	err := tree.Remove(4)
	assert.Nil(t, err)
	assert.Equal(t, 9, tree.Count())

	// remove another to make make sure
	err = tree.Remove(2)
	assert.Nil(t, err)
	assert.Equal(t, 8, tree.Count())

	// Lets add one back in
	_, err = tree.Insert(100, "one hundred")
	assert.Equal(t, 9, tree.Count())

	// Make a insert error checking that the count remains the same
	_, err = tree.Insert(3, "three")
	assert.Error(t, err)
	assert.Equal(t, 9, tree.Count())
}

func TestBTree_Flatten(t *testing.T) {
	tree := make_complext_tree()
	assert.Equal(t, 10, len(tree.Flatten()))
}
