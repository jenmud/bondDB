package index

import "fmt"

// newLeaf creates and returns a new empty Leaf.
func newLeaf() *Leaf {
	return new(Leaf)
}

// NewBTree create a new empty binary tree with root node starting
// with ID `0`.
func NewBTree() *BTree {
	b := BTree{
		root:  newLeaf(),
		count: 0, // count should be zero to exclude the default root
	}
	return &b
}

// Leaf represents a leaf in the binary tree.
type Leaf struct {
	// ID is a unique identification number for the leaf.
	ID uint64 `json: "id"`

	// Data is any value associated with the leaf.
	Data interface{} `json: "data,omitempty"`

	// Parent is the leafs parent leaf that it is attached to.
	Parent *Leaf `json: parent,omitempty`

	// Left is a leaf with a ID less then the current leafs ID.
	Left *Leaf `json: left,omitempty` // small

	// Right is a leaf with a ID greater then the current leafs ID.
	Right *Leaf `json: right,omitempty` // big
}

// BTree is a binary tree or nodes/leafs.
type BTree struct {
	// root is the starting leaf of the tree using the default
	// initialization values.
	root  *Leaf
	count int

	// Todo: Make this iterable
}

// insert is a helper function for inserting and updating leafs.
func insert(root, leaf *Leaf) (*Leaf, error) {
	if leaf == nil {
		return nil, fmt.Errorf("Leaf being inserted can not be nil")
	}

	if leaf.ID < root.ID {
		if root.Left == nil {
			leaf.Parent = root
			root.Left = leaf
			return leaf, nil
		} else {
			return insert(root.Left, leaf)
		}
	} else {
		if root.Right == nil {
			leaf.Parent = root
			root.Right = leaf
			return leaf, nil
		} else {
			return insert(root.Right, leaf)
		}
	}
}

// InsertLeaf inserts a leaf into the tree in the next logical place.
func (b *BTree) InsertLeaf(leaf *Leaf) (*Leaf, error) {
	if leaf.ID == 0 {
		return nil, fmt.Errorf("ID %d is reserved please use something else", leaf.ID)
	}

	leaf, err := insert(b.root, leaf)
	if err == nil {
		b.count++
	}
	return leaf, err
}

// Insert creates a new leaf with ID and data and inserts it into the
// tree in the next logical place.
func (b *BTree) Insert(id uint64, data interface{}) (*Leaf, error) {
	if !b.Contains(id) {
		leaf := newLeaf()
		leaf.ID = id
		leaf.Data = data
		return b.InsertLeaf(leaf)
	}
	return nil, fmt.Errorf("Leaf with ID %d already exists", id)
}

// Remove removes a leaf with matching ID from the binary tree re-arranging
// the leafs left and right leafs to the next logical place in the binary tree.
func (b *BTree) Remove(id uint64) error {
	leaf, err := b.GetLeaf(id)
	if err != nil {
		return fmt.Errorf("No such leaf with ID %d", id)
	}

	// if the leaf has no left or right leafs then
	// it is at the end and safe to remove from its
	// parent.
	if leaf.Left == nil && leaf.Right == nil {
		if leaf.ID < leaf.Parent.ID {
			leaf.Parent.Left = nil
			b.count--
			return nil
		} else {
			leaf.Parent.Right = nil
			b.count--
			return nil
		}
	}

	if leaf.Right != nil {
		leaf.Parent.Right = nil
		b.InsertLeaf(leaf.Right)
		// Insert will bump up the count, so make
		// sure that you decrement it again.
		b.count--
	}

	if leaf.Left != nil {
		leaf.Parent.Left = nil
		b.InsertLeaf(leaf.Left)
		// Insert will bump up the count, so make
		// sure that you decrement it again.
		b.count--
	}

	// Finally this decrement is for reals.
	b.count--
	return nil
}

// GetRoot returns the root leaf of the binary tree.
func (b *BTree) GetRoot() *Leaf {
	return b.root
}

// get is a internal helper function for searching the binary tree for a
// leaf matching the given ID.
func get(leaf *Leaf, id uint64) (*Leaf, error) {
	if leaf == nil {
		return nil, fmt.Errorf("Could not find leaf with ID %d", id)
	} else if id == leaf.ID {
		return leaf, nil
	} else if id < leaf.ID {
		return get(leaf.Left, id)
	} else {
		return get(leaf.Right, id)
	}
}

// Get searches the binary tree for a leaf matching the given ID and returns
// the leafs data associated with the found leaf.
func (b *BTree) Get(id uint64) (interface{}, error) {
	leaf, err := get(b.root, id)
	if err != nil {
		return nil, err
	}
	return leaf.Data, nil
}

// GetLeaf searches the binary tree for a leaf matching the given ID and
// returns the leaf if found.
func (b *BTree) GetLeaf(id uint64) (*Leaf, error) {
	return get(b.root, id)
}

// Contains returns true if the binary contains a leaf with the given ID.
func (b *BTree) Contains(id uint64) bool {
	_, err := b.Get(id)
	return err == nil
}

// Count returns the total leaf count.
func (b *BTree) Count() int {
	return b.count
}

func walk(leaf *Leaf, seen *[]*Leaf) {
	if leaf.Parent != nil && leaf.Parent != leaf {
		*seen = append(*seen, leaf)
	}

	if leaf.Left != nil {
		walk(leaf.Left, seen)
	}

	if leaf.Right != nil {
		walk(leaf.Right, seen)
	}
}

// Flatten flattens the binary tree into a slice parsing the tree
// left to right.
func (b *BTree) Flatten() []*Leaf {
	items := []*Leaf{}
	walk(b.root, &items)
	return items
}
