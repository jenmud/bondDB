package index

import "fmt"

func NewSimpleHash() *SimpleHash {
	sh := SimpleHash{
		items: make(map[uint64]*Item),
	}

	return &sh
}

type Item struct {
	ID   uint64
	Data interface{}
}

type SimpleHash struct {
	items map[uint64]*Item
}

func (sh *SimpleHash) Insert(id uint64, data interface{}) error {
	if !sh.Contains(id) {
		sh.items[id] = &Item{ID: id, Data: data}
		return nil
	}
	return fmt.Errorf("Item with ID %d already exists", id)
}

func (sh *SimpleHash) Contains(id uint64) bool {
	_, ok := sh.items[id]
	return ok
}

func (sh *SimpleHash) Remove(id uint64) error {
	if !sh.Contains(id) {
		return fmt.Errorf("Could not find item with ID %d", id)
	}
	delete(sh.items, id)
	return nil
}

func (sh *SimpleHash) GetItem(id uint64) (*Item, error) {
	item, ok := sh.items[id]
	if !ok {
		return nil, fmt.Errorf("Could not find item with ID %d", id)
	}
	return item, nil
}

func (sh *SimpleHash) Get(id uint64) (interface{}, error) {
	item, err := sh.GetItem(id)
	if err != nil {
		return nil, err
	}
	return item.Data, nil
}

func (sh *SimpleHash) Count() int {
	return len(sh.items)
}

func (sh *SimpleHash) Flatten() []*Item {
	items := make([]*Item, sh.Count())
	count := 0
	for _, value := range sh.items {
		items[count] = value
		count++
	}
	return items
}
