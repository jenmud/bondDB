package index

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSimpleHash_Insert(t *testing.T) {
	sh := NewSimpleHash()
	err := sh.Insert(1, "one")
	assert.Nil(t, err)
	assert.Equal(t, true, sh.Contains(1))

	err = sh.Insert(2, "two")
	assert.Nil(t, err)
	assert.Equal(t, true, sh.Contains(2))

	// Should get a error here
	err = sh.Insert(1, "dup one")
	assert.Error(t, err)
	// get item with ID 1 and check that the data was not updated
	data, err := sh.Get(1)
	assert.Nil(t, err)
	assert.Equal(t, "one", data)
}

func TestSimpleHash_Contains(t *testing.T) {
	sh := NewSimpleHash()
	err := sh.Insert(1, "one")
	assert.Nil(t, err)
	assert.Equal(t, true, sh.Contains(1))
	assert.Equal(t, false, sh.Contains(2))
}

func TestSimpleHash_Remove(t *testing.T) {
	sh := NewSimpleHash()
	err := sh.Insert(1, "one")
	assert.Nil(t, err)

	err = sh.Insert(2, "two")
	assert.Nil(t, err)

	err = sh.Remove(1)
	assert.Nil(t, err)
	assert.Equal(t, false, sh.Contains(1))

	err = sh.Remove(3)
	assert.Error(t, err)
}

func TestSimpleHash_Get(t *testing.T) {
	sh := NewSimpleHash()
	err := sh.Insert(1, "one")
	assert.Nil(t, err)

	data, err := sh.Get(1)
	assert.Nil(t, err)
	assert.Equal(t, "one", data)

	data, err = sh.Get(2)
	assert.Error(t, err)
	assert.Nil(t, data)
}

func TestSimpleHash_GetItem(t *testing.T) {
	sh := NewSimpleHash()
	err := sh.Insert(1, "one")
	assert.Nil(t, err)

	item, err := sh.GetItem(1)
	assert.Nil(t, err)
	assert.Equal(t, uint64(1), item.ID)
	assert.Equal(t, "one", item.Data)

	item, err = sh.GetItem(2)
	assert.Error(t, err)
	assert.Nil(t, item)
}

func TestSimpleHash_Count(t *testing.T) {
	sh := NewSimpleHash()

	err := sh.Insert(1, "one")
	assert.Nil(t, err)

	err = sh.Insert(2, "two")
	assert.Nil(t, err)

	err = sh.Insert(3, "three")
	assert.Nil(t, err)

	assert.Equal(t, 3, sh.Count())
}

func TestSimpleHash_Flatten(t *testing.T) {
	sh := NewSimpleHash()

	err := sh.Insert(1, "one")
	assert.Nil(t, err)

	err = sh.Insert(2, "two")
	assert.Nil(t, err)

	err = sh.Insert(3, "three")
	assert.Nil(t, err)

	items := sh.Flatten()

	assert.Equal(t, uint64(1), items[0].ID)
	assert.Equal(t, "one", items[0].Data)

	assert.Equal(t, uint64(2), items[1].ID)
	assert.Equal(t, "two", items[1].Data)

	assert.Equal(t, uint64(3), items[2].ID)
	assert.Equal(t, "three", items[2].Data)
}
