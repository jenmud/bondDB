package graph

import (
	"fmt"

	"gitlab.com/bondDB/graph/component"
	"gitlab.com/bondDB/graph/generators"
	"gitlab.com/bondDB/graph/index"
)

// Adder is an interface wrapping the graph add methods.
type Adder interface {

	// AddNode adds a node to the graph.
	AddNode(
		label string,
	) (component.NodeComponent, error)

	// AddEdge adds a edge between two node to the graph.
	AddEdge(
		head component.NodeComponent,
		label string,
		tail component.NodeComponent,
	) (component.EdgeComponent, error)
}

// Getter is an interface wrapping graph get methods.
type Getter interface {

	// GetNode returns a node.
	GetNode(id uint64) (component.NodeComponent, error)

	// GetNodes returns all the node graph components known to the graph.
	GetNodes() []component.NodeComponent

	// GetEdge returns a edge.
	GetEdge(id uint64) (component.EdgeComponent, error)

	// GetEdges returns all the edge graph components known to the graph.
	GetEdges() []component.EdgeComponent

	// GetNodesWithProperty returns all the nodes which which have the provided property key.
	GetNodesWithProperty(key string) []component.NodeComponent

	// GetNodesWithPropertyValue returns all the nodes which have the property value.
	GetNodesWithPropertyValue(value []byte) []component.NodeComponent

	// GetEdgesWithProperty returns all the nodes which which have the provided property key.
	GetEdgesWithProperty(key string) []component.EdgeComponent

	// GetEdgesWithPropertyValue returns all the edges which have the property value.
	GetEdgesWithPropertyValue(value []byte) []component.NodeComponent
}

// Setter is an interface wrapping graph set methods.
type Setter interface {

	// SetNodeProperty sets the property key and value for the provided graph component.
	SetNodeProperty(component component.NodeComponent, key string, value []byte) error

	// SetEdgeProperty sets the property key and value for the provided graph component.
	SetEdgeProperty(component component.EdgeComponent, key string, value []byte) error
}

// Remover is an interface wrapping graph remove methods.
type Remover interface {

	// RemoveNode removes a node from the graph.
	RemoveNode(id uint64) error

	// RemoveEdge removes a edge from the graph.
	RemoveEdge(id uint64) error
}

// Counter is an interface wrapping the graph count methods.
type Counter interface {
	// NodeCount returns the total count of nodes in the graph.
	NodeCount() int

	// EdgeCount returns the total count of edges in the graph.
	EdgeCount() int
}

// Finder is an interface wrapping methods used for searching the graph.
type Finder interface {
	// FindNodesByLabel searches the graph for node components by label.
	FindNodesByLabel(label string) []component.GraphComponent

	// FindEdgesByLabel searches the graph for edge components by label.
	FindEdgesByLabel(label string) []component.GraphComponent
}

// Grapher is an interface wrapping graph methods.
type Grapher interface {
	Adder
	Remover
	Getter
	Setter
	Counter
	Finder

	// HasNode returns True if there is a node with `id` in the graph.
	HasNode(id uint64) bool

	// HasEdge returns True if there is a edge with `id` in the graph.
	HasEdge(id uint64) bool
}

// NewGraph creates a empty graph.
func NewGraph(nodeIDgen, edgeIDgen generators.IdentityGenerator) *Graph {
	graph := Graph{
		nIDGen:             nodeIDgen,
		eIDGen:             edgeIDgen,
		nodeLabelIndex:     index.NewTTree(),
		edgeLabelIndex:     index.NewTTree(),
		nodePropertyIndex:  index.NewTTree(),
		nodePropValueIndex: index.NewTTree(),
		edgePropValueIndex: index.NewTTree(),
		edgePropertyIndex:  index.NewTTree(),
		nodes:              index.NewSimpleHash(),
		edges:              index.NewSimpleHash(),
	}
	return &graph
}

// Graph is a structure storing nodes and edges.
type Graph struct {
	nIDGen             generators.IdentityGenerator
	eIDGen             generators.IdentityGenerator
	nodeLabelIndex     *index.TTree
	edgeLabelIndex     *index.TTree
	nodePropertyIndex  *index.TTree
	nodePropValueIndex *index.TTree
	edgePropertyIndex  *index.TTree
	edgePropValueIndex *index.TTree
	nodes              *index.SimpleHash
	edges              *index.SimpleHash
}

// indexInsert is a helper function for inserting and updating the index with the graph component.
func indexInsert(inx *index.TTree, key string, comp component.GraphComponent) {
	data, err := inx.Get(key)
	if err != nil {
		set := make(map[uint64]component.GraphComponent)
		set[comp.GetID()] = comp
		inx.Insert(key, set)
	} else {
		nodeSet := data.(map[uint64]component.GraphComponent)
		if _, ok := nodeSet[comp.GetID()]; !ok {
			nodeSet[comp.GetID()] = comp
		}
	}
}

// indexRemove is an internal helper function for removing a graph component from the given index.
func indexRemove(inx *index.TTree, label string, comp component.GraphComponent) {
	data, err := inx.Get(label)
	if err == nil {
		set := data.(map[uint64]component.GraphComponent)
		if _, ok := set[comp.GetID()]; ok {
			set[comp.GetID()] = comp
			delete(set, comp.GetID())
			if len(set) == 0 {
				inx.Remove(label)
			}
		}
	}
}

// findNodes is a internal helper function for searching for graph components in a index.
func findNodes(inx *index.TTree, key string) []component.GraphComponent {
	found := []component.GraphComponent{}

	data, err := inx.Get(key)
	if err != nil {
		return []component.GraphComponent{}
	}

	set := data.(map[uint64]component.GraphComponent)
	for _, item := range set {
		found = append(found, item)
	}

	return found
}

// AddNode adds a node to the graph.
func (g *Graph) AddNode(label string) (component.NodeComponent, error) {
	node := component.NewNode(
		g.nIDGen.Next(),
		label,
	)

	id := node.GetID()
	if g.HasNode(id) {
		g.nIDGen.Rollback()
		return nil, fmt.Errorf("ID conflict %d", id)
	}

	err := node.Bind(g)
	if err != nil {
		g.nIDGen.Rollback()
		return nil, err
	}

	err = g.nodes.Insert(id, node)
	if err != nil {
		g.nIDGen.Rollback()
		return nil, err
	}

	indexInsert(g.nodeLabelIndex, label, node)
	return node, nil
}

// RemoveNode removes a node from the graph.
func (g *Graph) RemoveNode(id uint64) error {
	node, err := g.GetNode(id)
	if err != nil {
		return err
	}

	// make sure there are not edges linked to the node.
	if len(node.GetEdges()) > 0 {
		return fmt.Errorf(
			"Can not remove node when there are still edges attached to it",
		)
	}

	err = g.nodes.Remove(id)
	if err != nil {
		return err
	}

	indexRemove(g.nodeLabelIndex, node.GetLabel(), node)
	return nil
}

// GetNode returns a node.
func (g *Graph) GetNode(id uint64) (component.NodeComponent, error) {
	node, err := g.nodes.Get(id)
	if err != nil {
		return nil, fmt.Errorf("No such node with ID %d found", id)
	}
	return node.(component.NodeComponent), nil
}

// GetNodes returns all the node graph components known to the graph.
func (g *Graph) GetNodes() []component.NodeComponent {
	nodes := make([]component.NodeComponent, g.NodeCount())
	for count, item := range g.nodes.Flatten() {
		nodes[count] = item.Data.(component.NodeComponent)
	}
	return nodes
}

// NodeCount returns the total count of nodes in the graph.
func (g *Graph) NodeCount() int {
	return g.nodes.Count()
}

// HasNode returns True if there is a node with `id` in the graph.
func (g *Graph) HasNode(id uint64) bool {
	return g.nodes.Contains(id)
}

// FindNodesByLabel searches the graph for node components by label.
func (g *Graph) FindNodesByLabel(label string) []component.GraphComponent {
	return findNodes(g.nodeLabelIndex, label)
}

// AddEdge adds a edge between two node to the graph.
func (g *Graph) AddEdge(
	head component.NodeComponent,
	label string,
	tail component.NodeComponent,
) (component.EdgeComponent, error) {
	if !g.HasNode(head.GetID()) {
		return nil, fmt.Errorf("Head node %d not found", head.GetID())
	}

	if !g.HasNode(tail.GetID()) {
		return nil, fmt.Errorf("Tail node %d not found", head.GetID())
	}

	edge := component.NewEdge(
		g.eIDGen.Next(),
		head,
		label,
		tail,
	)

	id := edge.GetID()
	if g.HasEdge(id) {
		g.eIDGen.Rollback()
		return nil, fmt.Errorf("ID conflict %d", id)
	}

	err := edge.Bind(g)
	if err != nil {
		g.eIDGen.Rollback()
		return nil, err
	}

	// link the nodes together
	err = head.Link(edge)
	if err != nil {
		g.eIDGen.Rollback()
		return nil, err
	}

	// Do the inverse link
	err = tail.Link(edge)
	if err != nil {
		head.Unlink(edge)
		g.eIDGen.Rollback()
		return nil, err
	}

	err = g.edges.Insert(id, edge)
	if err != nil {
		head.Unlink(edge)
		tail.Unlink(edge)
		g.eIDGen.Rollback()
		return nil, err
	}

	indexInsert(g.edgeLabelIndex, label, edge)
	return edge, nil
}

// RemoveEdge removes a edge from the graph.
func (g *Graph) RemoveEdge(id uint64) error {
	edge, err := g.GetEdge(id)
	if err != nil {
		return err
	}

	head := edge.GetHead()
	tail := edge.GetTail()

	// Unlink the nodes
	err = head.Unlink(edge)
	if err != nil {
		return err
	}

	// Do the inverse unlink
	err = tail.Unlink(edge)
	if err != nil {
		return err
	}

	// Todo remove should remove from the index too!
	err = g.edges.Remove(id)
	if err != nil {
		return err
	}

	indexRemove(g.edgeLabelIndex, edge.GetLabel(), edge)
	return nil
}

// GetEdge returns a edge.
func (g *Graph) GetEdge(id uint64) (component.EdgeComponent, error) {
	edge, err := g.edges.Get(id)
	if err != nil {
		return nil, fmt.Errorf("No such edge with ID %d found", id)
	}
	return edge.(component.EdgeComponent), nil
}

// GetEdges returns all the edge graph components known to the graph.
func (g Graph) GetEdges() []component.EdgeComponent {
	edges := make([]component.EdgeComponent, g.EdgeCount())
	for count, item := range g.edges.Flatten() {
		edges[count] = item.Data.(component.EdgeComponent)
	}
	return edges
}

// EdgeCount returns the total count of edges in the graph.
func (g Graph) EdgeCount() int {
	return g.edges.Count()
}

// HasEdge returns True if there is a edge with `id` in the graph.
func (g *Graph) HasEdge(id uint64) bool {
	return g.edges.Contains(id)
}

// FindEdgesByLabel searches the graph for edge components by label.
func (g *Graph) FindEdgesByLabel(label string) []component.GraphComponent {
	return findNodes(g.edgeLabelIndex, label)
}

// GetNodesWithProperty returns all the nodes which which have the provided property key.
func (g *Graph) GetNodesWithProperty(key string) []component.NodeComponent {
	indexed := findNodes(g.nodePropertyIndex, key)
	nodes := []component.NodeComponent{}
	for _, item := range indexed {
		nodes = append(nodes, item.(component.NodeComponent))
	}
	return nodes
}

// GetNodesWithPropertyValue returns all the nodes which have the property value.
func (g *Graph) GetNodesWithPropertyValue(value []byte) []component.NodeComponent {
	indexed := findNodes(g.nodePropValueIndex, string(value))
	nodes := []component.NodeComponent{}
	for _, item := range indexed {
		nodes = append(nodes, item.(component.NodeComponent))
	}
	return nodes
}

// GetEdgesWithProperty returns all the nodes which have the provided property key.
func (g *Graph) GetEdgesWithProperty(key string) []component.EdgeComponent {
	indexed := findNodes(g.edgePropertyIndex, key)
	edges := []component.EdgeComponent{}
	for _, item := range indexed {
		edges = append(edges, item.(component.EdgeComponent))
	}
	return edges
}

// GetEdgesWithPropertyValue returns all the edges which have the property value.
func (g *Graph) GetEdgesWithPropertyValue(value []byte) []component.EdgeComponent {
	indexed := findNodes(g.edgePropValueIndex, string(value))
	edges := []component.EdgeComponent{}
	for _, item := range indexed {
		edges = append(edges, item.(component.EdgeComponent))
	}
	return edges
}

// SetNodeProperty sets the property key and value for the provided graph component.
func (g *Graph) SetNodeProperty(component component.NodeComponent, key string, value []byte) error {
	indexInsert(g.nodePropertyIndex, key, component)
	indexInsert(g.nodePropValueIndex, string(value), component)
	return nil
}

// SetEdgeProperty sets the property key and value for the provided graph component.
func (g *Graph) SetEdgeProperty(component component.EdgeComponent, key string, value []byte) error {
	indexInsert(g.edgePropertyIndex, key, component)
	indexInsert(g.edgePropValueIndex, string(value), component)
	return nil
}
