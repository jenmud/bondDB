package component

import (
	"fmt"

	"gitlab.com/bondDB/graph/index"
)

// NodeComponent is an interface wrapping all method needed
// by a node graph component.
type NodeComponent interface {
	GraphComponent

	// Link links the head and tail graph components to each other.
	Link(edge EdgeComponent) error

	// Unlink remove the link between head and tail graph components.
	Unlink(edge EdgeComponent) error

	// GetInEdges returns all the inbound edge components
	GetInEdges() []EdgeComponent

	// GetOutEdges returns all the outbound edge components
	GetOutEdges() []EdgeComponent

	// GetEdges returns all the edge components (both in and outbound)
	GetEdges() []EdgeComponent

	// GetInNodes returns all the inbound nodes.
	GetInNodes() []NodeComponent

	// GetOutNodes returns all the outbound nodes.
	GetOutNodes() []NodeComponent

	// GetNodes returns all the linked nodes (outbound and inbound).
	GetNodes() []NodeComponent
}

// NewNode returns a empty node.
func NewNode(
	id uint64,
	label string,
) *Node {
	node := Node{
		baseComponent: baseComponent{
			ID:         id,
			Label:      label,
			Properties: index.NewTTree(),
		},
		inEdges:  index.NewBTree(),
		outEdges: index.NewBTree(),
	}
	return &node
}

// Node is a component of a graph.
type Node struct {
	baseComponent
	inEdges  *index.BTree
	outEdges *index.BTree
}

// Link links the head and tail graph components to each other.
func (n *Node) Link(edge EdgeComponent) error {
	var tree *index.BTree

	if edge.GetHead().GetID() == n.ID {
		tree = n.outEdges

	} else if edge.GetTail().GetID() == n.ID {
		tree = n.inEdges

	} else {
		return fmt.Errorf("Node is not a head or tail node of the edge")
	}

	if tree.Contains(edge.GetID()) {
		return fmt.Errorf("Edge '%d' already exists", edge.GetID())
	}

	_, err := tree.Insert(edge.GetID(), edge)
	return err
}

// Unlink remove the link between head and tail graph components.
func (n *Node) Unlink(edge EdgeComponent) error {
	var tree *index.BTree

	if edge.GetHead().GetID() == n.ID {
		tree = n.outEdges

	} else if edge.GetTail().GetID() == n.ID {
		tree = n.inEdges

	} else {
		return fmt.Errorf("Node is not a head or tail node of the edge")
	}

	if !tree.Contains(edge.GetID()) {
		return fmt.Errorf("Edge '%d' does not exist", edge.GetID())
	}

	return tree.Remove(edge.GetID())
}

// GetInEdges returns all the inbound edge components
func (n Node) GetInEdges() []EdgeComponent {
	edges := make([]EdgeComponent, n.inEdges.Count())
	count := 0
	for _, leaf := range n.inEdges.Flatten() {
		edges[count] = leaf.Data.(EdgeComponent)
		count++
	}
	return edges
}

// GetOutEdges returns all the outbound edge components
func (n Node) GetOutEdges() []EdgeComponent {
	edges := make([]EdgeComponent, n.outEdges.Count())
	count := 0
	for _, leaf := range n.outEdges.Flatten() {
		edges[count] = leaf.Data.(EdgeComponent)
		count++
	}
	return edges
}

// GetEdges returns all the edge components (both in and outbound)
func (n Node) GetEdges() []EdgeComponent {
	edges := []EdgeComponent{}

	for _, each := range n.GetInEdges() {
		edges = append(edges, each)
	}

	for _, each := range n.GetOutEdges() {
		edges = append(edges, each)
	}

	return edges
}

// GetInNodes returns all the inbound nodes.
func (n Node) GetInNodes() []NodeComponent {
	edges := n.GetInEdges()
	nodes := make([]NodeComponent, len(edges))
	for count, edge := range edges {
		nodes[count] = edge.GetHead()
	}
	return nodes
}

// GetOutNodes returns all the outbound nodes.
func (n Node) GetOutNodes() []NodeComponent {
	edges := n.GetOutEdges()
	nodes := make([]NodeComponent, len(edges))
	for count, edge := range edges {
		nodes[count] = edge.GetTail()
	}
	return nodes
}

// GetNodes returns all the linked nodes (outbound and inbound).
func (n Node) GetNodes() []NodeComponent {
	in := n.GetInEdges()
	out := n.GetOutEdges()
	nodes := make([]NodeComponent, len(in)+len(out))
	count := 0

	for _, edge := range in {
		nodes[count] = edge.GetHead()
		count++
	}

	for _, edge := range out {
		nodes[count] = edge.GetTail()
		count++
	}

	return nodes
}

// SetProperty sets and map property key with value, updating if key has already got a value.
func (n *Node) SetProperty(key string, value []byte) error {

	if n.IsBound() {
		err := n.graph.SetNodeProperty(n, key, value)
		if err != nil {
			return err
		}
	}

	_, err := n.Properties.Insert(key, value)
	return err

}
