package component

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewEdge(t *testing.T) {
	head, tail := makeHeadTailNodes()
	edge := NewEdge(0, head, "KNOWS", tail)
	assert.Implements(t, (*GraphComponent)(nil), edge)
	assert.Implements(t, (*EdgeComponent)(nil), edge)
}

func TestEdgeGetID(t *testing.T) {
	head, tail := makeHeadTailNodes()
	edge := NewEdge(0, head, "KNOWS", tail)
	assert.Equal(t, uint64(0), edge.GetID())
}

func TestEdgeGetLabel(t *testing.T) {
	head, tail := makeHeadTailNodes()
	edge := NewEdge(0, head, "KNOWS", tail)
	assert.Equal(t, "KNOWS", fmt.Sprintf("%s", edge.GetLabel()))
}

func TestEdgeGetHead(t *testing.T) {
	head, tail := makeHeadTailNodes()
	edge := NewEdge(0, head, "KNOWS", tail)
	assert.Equal(t, head, edge.GetHead())
}

func TestEdgeGetTail(t *testing.T) {
	head, tail := makeHeadTailNodes()
	edge := NewEdge(0, head, "KNOWS", tail)
	assert.Equal(t, tail, edge.GetTail())
}

func TestEdge_GetProperty(t *testing.T) {
	foo := NewNode(0, "PERSON")
	bar := NewNode(1, "PERSON")

	edge := NewEdge(0, foo, "KNOWS", bar)

	err := edge.SetProperty("Since", []byte("school"))
	assert.Nil(t, err)

	err = edge.SetProperty("Are", []byte("Friends"))
	assert.Nil(t, err)

	data, err := edge.GetProperty("Since")
	assert.Nil(t, err)
	assert.Equal(t, "school", string(data))

	data, err = edge.GetProperty("Are")
	assert.Nil(t, err)
	assert.Equal(t, "Friends", string(data))

	data, err = edge.GetProperty("not exists")
	assert.NotNil(t, err)
	assert.Nil(t, data)
}

func TestEdge_SetProperty(t *testing.T) {
	foo := NewNode(0, "PERSON")
	bar := NewNode(1, "PERSON")

	edge := NewEdge(0, foo, "KNOWS", bar)

	err := edge.SetProperty("Since", []byte("school"))
	assert.Nil(t, err)

	data, err := edge.GetProperty("Since")
	assert.Nil(t, err)
	assert.Equal(t, "school", string(data))
}
