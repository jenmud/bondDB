package component

import (
	"fmt"

	"gitlab.com/bondDB/graph/index"
)

// PropertyGetter is an interface wrapping get methods.
type PropertyGetter interface {
	// GetProperty gets the data associated with property key.
	GetProperty(key string) ([]byte, error)
}

// PropertySetter is an interface wrapping set methods.
type PropertySetter interface {
	// SetProperty sets and maps property key with value,
	// updating if key has already got a value.
	SetProperty(key string, value []byte) error
}

// Binder is a interface that wraps methods
// for binding and checking if bound.
type Binder interface {
	// IsBound returns true if the graph component is
	// bound to a graph.
	IsBound() bool

	// Bind binds with item.
	Bind(item GraphComponentSetter) error
}

// GraphComponent is a interface wrapping methods for
// components of a graph.
type GraphComponent interface {
	PropertyGetter
	PropertySetter
	Binder

	// GetID returns the components unique identifier.
	GetID() uint64

	// GetLabel returns the components label.
	GetLabel() string
}

// GraphComponentSetter is an interface wrapping graph methods.
type GraphComponentSetter interface {

	// SetNodeProperty sets the property key and value for the provided graph component.
	SetNodeProperty(component NodeComponent, key string, value []byte) error

	// SetEdgeProperty sets the property key and value for the provided graph component.
	SetEdgeProperty(component EdgeComponent, key string, value []byte) error
}

type baseComponent struct {
	// ID unique identifier number.
	ID uint64

	// Label associated with the Node.
	Label string

	// graph is the graph that is bound with this component.
	graph GraphComponentSetter

	// Properties are the components properties as key/value pairs.
	Properties *index.TTree
}

// GetID returns the components unique identifier.
func (b *baseComponent) GetID() uint64 {
	return b.ID
}

// GetLabel returns the components label.
func (b *baseComponent) GetLabel() string {
	return b.Label
}

// IsBound returns true if the graph component is
// bound to a graph.
func (b *baseComponent) IsBound() bool {
	return b.graph != nil
}

// Bind binds with item.
func (b *baseComponent) Bind(item GraphComponentSetter) error {
	if b.IsBound() {
		return fmt.Errorf("Component is already bound")
	}
	b.graph = item
	return nil
}

// GetProperty gets the property data associated with property key.
func (b *baseComponent) GetProperty(key string) ([]byte, error) {
	value, err := b.Properties.Get(key)
	if err != nil {
		return nil, fmt.Errorf("No such property key %q", key)
	}
	return value.([]byte), nil
}
