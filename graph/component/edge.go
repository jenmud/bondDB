package component

import (
	"gitlab.com/bondDB/graph/index"
)

// EdgeComponent is a interface wrapping methods relating to
// a edge linking two graph components.
type EdgeComponent interface {
	GraphComponent
	GetHead() NodeComponent
	GetTail() NodeComponent
}

// NewEdge returns a empty node.
func NewEdge(
	id uint64,
	head NodeComponent,
	label string,
	tail NodeComponent,
) *Edge {
	edge := Edge{
		baseComponent: baseComponent{
			ID:         id,
			Label:      label,
			Properties: index.NewTTree(),
		},
		Head: head,
		Tail: tail,
	}
	return &edge
}

// Edge is a component used for linking two graph components together.
type Edge struct {
	baseComponent
	Head, Tail NodeComponent
}

// GetHead returns the head graph component of the edge
// (head)-->(tail)
func (e *Edge) GetHead() NodeComponent {
	return e.Head
}

// GetTail returns the tail graph component of the edge
// (head)-->(tail)
func (e *Edge) GetTail() NodeComponent {
	return e.Tail
}

// SetProperty sets and map property key with value, updating if key has already got a value.
func (e *Edge) SetProperty(key string, value []byte) error {

	if e.IsBound() {
		err := e.graph.SetEdgeProperty(e, key, value)
		if err != nil {
			return err
		}
	}

	_, err := e.Properties.Insert(key, value)
	return err

}
