package component

import "fmt"

type mockGraph struct {
	key   string
	value []byte
}

func (m *mockGraph) GetProperty(component GraphComponent, key string) ([]byte, error) {
	if key == m.key {
		return m.value, nil
	}
	return nil, fmt.Errorf("No key found")
}

func (m *mockGraph) SetProperty(component GraphComponent, key string, value []byte) error {
	if m.key == key {
		m.value = value
		return nil
	}
	return fmt.Errorf("Some error happened")
}

func makeNewMockGraph(propKey string, propValue []byte) *mockGraph {
	return &mockGraph{
		key:   propKey,
		value: propValue,
	}
}
func makeHeadTailNodes() (head, tail NodeComponent) {
	head = NewNode(0, "PERSON")
	tail = NewNode(1, "PERSON")
	return head, tail
}
