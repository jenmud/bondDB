package component

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func ExampleNewNode() {
	node := NewNode(0, "PERSON")
	node.SetProperty("Name", []byte("Foo"))
	node.SetProperty("Surname", []byte("Bar"))
	fmt.Printf("I have a new node %s", node)
}

func ExampleNode_Link() {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")
	edge := NewEdge(1, foo, "knows", bar)
	foo.Link(edge)
}

func TestNewNode(t *testing.T) {
	node := NewNode(1, "PERSON")
	assert.Implements(t, (*GraphComponent)(nil), node)
}

func TestNodeGetID(t *testing.T) {
	node := NewNode(1, "PERSON")
	assert.Equal(t, uint64(1), node.GetID())
}

func TestNodeGetLabel(t *testing.T) {
	node := NewNode(1, "PERSON")
	assert.Equal(t, "PERSON", fmt.Sprintf("%s", node.GetLabel()))
}

func TestNode_LinkOutbound(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")

	// ( foo ) --> ( bar )
	// foo would be outbound to bar
	edge := NewEdge(1, foo, "KNOWS", bar)
	err := foo.Link(edge)
	assert.Nil(t, err)

	assert.Equal(t, 0, foo.inEdges.Count())
	assert.Equal(t, 1, foo.outEdges.Count())
}

func TestNode_UnlinkOutbound(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")

	// ( foo ) --> ( bar )
	// foo would be outbound to bar
	edge := NewEdge(1, foo, "KNOWS", bar)
	err := foo.Link(edge)
	assert.Nil(t, err)

	// remove the link
	err = foo.Unlink(edge)
	assert.Equal(t, 0, foo.inEdges.Count())
	assert.Equal(t, 0, foo.outEdges.Count())
}

func TestNode_LinkInbound(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")

	// ( bar ) --> ( foo )
	// bar would be inbound to foo
	edge := NewEdge(1, bar, "KNOWS", foo)
	err := foo.Link(edge)
	assert.Nil(t, err)
	assert.Equal(t, 1, foo.inEdges.Count())
	assert.Equal(t, 0, foo.outEdges.Count())
}

func TestNode_UnlinkInbound(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")

	// ( bar ) --> ( foo )
	// bar would be inbound to foo
	edge := NewEdge(1, bar, "KNOWS", foo)
	foo.Link(edge)

	// remove outbound edge
	err := foo.Unlink(edge)
	assert.Nil(t, err)
	assert.Equal(t, 0, foo.inEdges.Count())
	assert.Equal(t, 0, foo.outEdges.Count())
}

func TestNode_LinkUnrelatedEdge(t *testing.T) {
	foo := NewNode(1, "PERSON")
	dogs := NewNode(2, "ANIMAL")
	cats := NewNode(2, "ANIMAL")

	edge := NewEdge(1, dogs, "HATES", cats)
	err := foo.Link(edge)
	assert.Error(t, err)
}

func TestNode_UnlinkUnrelatedEdge(t *testing.T) {
	foo := NewNode(1, "PERSON")
	dogs := NewNode(2, "ANIMAL")
	cats := NewNode(3, "ANIMAL")

	edge := NewEdge(1, dogs, "HATES", cats)
	err := foo.Unlink(edge)
	assert.Error(t, err)
}

func TestNode_UnlinkUnknownEdge(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")

	edge := NewEdge(1, foo, "HATES", bar)
	err := foo.Unlink(edge)
	assert.Error(t, err)
}

func TestNode_LinkDoubleLink(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")

	edge := NewEdge(1, bar, "KNOWS", foo)
	foo.Link(edge)
	err := foo.Link(edge)
	assert.Error(t, err)
}

func TestNode_GetInEdges(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")
	john := NewNode(3, "PERSON")

	// ( foo ) --> ( bar )
	// foo would be outbound to bar
	foobar := NewEdge(1, foo, "KNOWS", bar)

	// ( john ) --> ( foo )
	// john would be inbound to foo
	johnfoo := NewEdge(2, john, "KNOWS", foo)

	foo.Link(foobar)
	foo.Link(johnfoo)

	assert.Equal(t, 1, len(foo.GetInEdges()))
	assert.Equal(t, []EdgeComponent{johnfoo}, foo.GetInEdges())
}

func TestNode_GetOutEdges(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")
	john := NewNode(3, "PERSON")

	// ( foo ) --> ( bar )
	// foo would be outbound to bar
	foobar := NewEdge(1, foo, "KNOWS", bar)

	// ( john ) --> ( foo )
	// john would be inbound to foo
	johnfoo := NewEdge(2, john, "KNOWS", foo)

	foo.Link(foobar)
	foo.Link(johnfoo)

	assert.Equal(t, 1, len(foo.GetOutEdges()))
	assert.Equal(t, []EdgeComponent{foobar}, foo.GetOutEdges())
}

func TestNode_GetEdges(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")
	john := NewNode(3, "PERSON")

	// ( foo ) --> ( bar )
	// foo would be outbound to bar
	foobar := NewEdge(1, foo, "KNOWS", bar)

	// ( john ) --> ( foo )
	// john would be inbound to foo
	johnfoo := NewEdge(2, john, "KNOWS", foo)

	foo.Link(foobar)
	foo.Link(johnfoo)

	assert.Equal(t, 2, len(foo.GetEdges()))
	assert.Equal(t, []EdgeComponent{johnfoo, foobar}, foo.GetEdges())
}

func TestNode_GetInNodes(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")
	john := NewNode(3, "PERSON")

	// ( foo ) --> ( bar )
	// foo would be outbound to bar
	foobar := NewEdge(1, foo, "KNOWS", bar)

	// ( john ) --> ( foo )
	// john would be inbound to foo
	johnfoo := NewEdge(2, john, "KNOWS", foo)

	foo.Link(foobar)
	foo.Link(johnfoo)

	assert.Equal(t, 1, len(foo.GetInNodes()))
	assert.Equal(t, []NodeComponent{john}, foo.GetInNodes())
}

func TestNode_GetOutNodes(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")
	john := NewNode(3, "PERSON")

	// ( foo ) --> ( bar )
	// foo would be outbound to bar
	foobar := NewEdge(1, foo, "KNOWS", bar)

	// ( john ) --> ( foo )
	// john would be inbound to foo
	johnfoo := NewEdge(1, john, "KNOWS", foo)

	foo.Link(foobar)
	foo.Link(johnfoo)

	assert.Equal(t, 1, len(foo.GetOutNodes()))
	assert.Equal(t, []NodeComponent{bar}, foo.GetOutNodes())
}

func TestNode_GetNodes(t *testing.T) {
	foo := NewNode(1, "PERSON")
	bar := NewNode(2, "PERSON")
	john := NewNode(3, "PERSON")

	// ( foo ) --> ( bar )
	// foo would be outbound to bar
	foobar := NewEdge(1, foo, "KNOWS", bar)

	// ( john ) --> ( foo )
	// john would be inbound to foo
	johnfoo := NewEdge(2, john, "KNOWS", foo)

	foo.Link(foobar)
	foo.Link(johnfoo)

	assert.Equal(t, 2, len(foo.GetNodes()))
	assert.Equal(t, []NodeComponent{john, bar}, foo.GetNodes())
}

func TestNode_IsBound(t *testing.T) {
	foo := NewNode(1, "PERSON")
	assert.Equal(t, false, foo.IsBound())

	graph := new(mockGraph)
	foo.Bind(graph)
	assert.Equal(t, true, foo.IsBound())
}

func TestNode_Bind(t *testing.T) {
	graph := new(mockGraph)
	foo := NewNode(1, "PERSON")
	err := foo.Bind(graph)
	assert.Nil(t, err)
	assert.Equal(t, true, foo.IsBound())

	err = foo.Bind(graph) // Should error because you bind above
	assert.NotNil(t, err)
}

func TestNode_GetProperty(t *testing.T) {
	foo := NewNode(1, "PERSON")
	foo.SetProperty("Name", []byte("Foo"))
	foo.SetProperty("Age", []byte("21"))

	data, err := foo.GetProperty("Name")
	assert.Nil(t, err)
	assert.Equal(t, "Foo", string(data))

	data, err = foo.GetProperty("Age")
	assert.Nil(t, err)
	assert.Equal(t, "21", string(data))

	data, err = foo.GetProperty("not exists")
	assert.NotNil(t, err)
	assert.Nil(t, data)
}

func TestNode_SetProperty(t *testing.T) {
	foo := NewNode(1, "PERSON")

	err := foo.SetProperty("Name", []byte("Foo"))
	assert.Nil(t, err)

	data, err := foo.GetProperty("Name")
	assert.Equal(t, "Foo", string(data))
}
