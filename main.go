package main

import (
	"fmt"

	"gitlab.com/bondDB/graph"
	"gitlab.com/bondDB/graph/generators"
)

func main() {
	nIDGen := generators.NewIdentityGenerator(0)
	eIDGen := generators.NewIdentityGenerator(0)

	g := graph.NewGraph(nIDGen, eIDGen)

	n, _ := g.AddNode("person")
	n.SetProperty("name", []byte("foo"))
	n.SetProperty("age", []byte("37"))
	n.SetProperty("sex", []byte("male"))

	b, _ := g.AddNode("person")
	b.SetProperty("name", []byte("bar"))
	b.SetProperty("age", []byte("21"))

	d, _ := g.AddNode("dog")
	d.SetProperty("name", []byte("socks"))

	e, _ := g.AddEdge(n, "KNOWS", b)
	e.SetProperty("since", []byte("school"))

	g.AddEdge(b, "OWNS", d)
	g.AddEdge(n, "LIKES", d)
	g.AddEdge(d, "LIKES", n)

	fmt.Printf("Dogs: %d\n", len(g.FindNodesByLabel("dog")))
	fmt.Printf("Persons: %d\n", len(g.FindNodesByLabel("person")))
	fmt.Printf("Total nodes: %d\n", g.NodeCount())
	fmt.Printf("Total edges: %d\n", g.EdgeCount())

	fmt.Printf("Nodes with %s properties: %d\n", "name", len(g.GetNodesWithProperty("name")))
	fmt.Printf("Edges with %s properties: %d\n", "since", len(g.GetEdgesWithProperty("since")))
	fmt.Printf("Nodes with property value %s: %d\n", "sex", len(g.GetNodesWithPropertyValue([]byte("male"))))
	fmt.Printf("Nodes with property value %s: %d\n", "school", len(g.GetEdgesWithPropertyValue([]byte("school"))))
}
